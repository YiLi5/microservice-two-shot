function ShoeList({shoes}) {

  async function handleDelete (shoeId){
    console.log(shoeId)
    const shoeUrl = `http://localhost:8080/api/shoes/${shoeId}/`
    const fetchConfig = {
    method: "delete",
    }
  const response = await fetch(shoeUrl, fetchConfig)
  window.location.reload();
  }

  return (
      <>
        <table className="table table-striped align-middle mt-5">
          <thead>
            <tr>
              <th>Model</th>
              <th>Manufacturer</th>
              <th>Color</th>
              <th>Picture</th>
              <th>Bin</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {shoes.map((shoe) => {
              return (
                <tr key={shoe.id}>
                  <td>{ shoe.model_name }</td>
                  <td>{ shoe.manufacturer }</td>
                  <td>{ shoe.color }</td>
                  <td><img src={shoe.picture_url} className="img-thumbnail" width="150" height="150"></img></td>
                  <td>{shoe.bin}</td>
                  <td>
                    <button type="button" value={shoe.id} onClick={() => handleDelete(shoe.id)}>Delete</button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </>
      );
  }

  export default ShoeList;
