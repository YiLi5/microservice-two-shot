import django
import os
import sys
import time
import json
import requests

# Append an empty string to the system path. This is necessary to ensure that the hats_rest app can be found by Django.
sys.path.append("")
# Set the DJANGO_SETTINGS_MODULE environment variable to "hats_project.settings", which specifies the settings module that Django should use.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
# Call django.setup() to configure Django's settings and initialize any necessary components.
django.setup()

# Import models from hats_rest, here.
# from hats_rest.models import Something
from hats_rest.models import LocationVO

# Define a function called get_location() that retrieves data from an external API and updates or creates LocationVO objects based on the data.
def get_location():
    # Send an HTTP GET request to the "http://wardrobe-api:8000/api/locations/" URL using the requests library and store the response in the response variable.
    response = requests.get("http://wardrobe-api:8000/api/locations/")
    # Decode the JSON content of the response using the json module and store it in the content variable.
    content = json.loads(response.content)
    print(content)
    # Loop through the locations in the response and call the update_or_create() method on the LocationVO.objects manager to create new LocationVO objects or update existing ones based on the import_href value
    for location in content["locations"]:
        LocationVO.objects.update_or_create(
            import_href=location["href"],
            # Pass the defaults argument to update_or_create() with the values to set for the other fields of the LocationVO object. This uses the location data from the external API.
            defaults = {
                "closet_name": location["closet_name"],
                "section_number": location["section_number"],
                "shelf_number": location["shelf_number"],
            },
        )


def poll():
    while True:
        print('Hats poller polling for data')
        try:
            get_location()
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)




if __name__ == "__main__":
    poll()
